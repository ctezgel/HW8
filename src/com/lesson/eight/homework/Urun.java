package com.lesson.eight.homework;

public class Urun {
	
	private double unitPrice;
	private String unitName;
	private int quantity;
	
	public Urun(String unitName, double unitPrice, int quantity) {
		setUnitName(unitName);
		setUnitPrice(unitPrice);
		setQuantity(quantity);
	}

	public double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(double unitPrice) {
		this.unitPrice = unitPrice;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public double sellUrun(int howManySell) {
		if(howManySell < getQuantity()) {
			setQuantity(getQuantity() - howManySell);
			return (howManySell * getUnitPrice());
		}
		return 0;
	}
}
