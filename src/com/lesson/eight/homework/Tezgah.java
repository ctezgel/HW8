package com.lesson.eight.homework;

public class Tezgah {
	private Urun[] units;
	private double cash;

	public Urun[] getUnits() {
		return units;
	}

	public void setUnits(Urun[] units) {
		this.units = units;
	}

	public double getCash() {
		return cash;
	}

	public void setCash(double cash) {
		this.cash = cash;
	}
	
	public Tezgah(double cash) {
		setCash(cash);
	}
	
	public Tezgah(Urun[] units, double cash) {
		this(cash);
		setUnits(units);
		
	}

	public Urun getProductByIndex(int index) {
		if(index >= units.length ) {
			return null;
		}
		return units[index];
	}
	
	public String getProductNameByIndex(int index) {
		
		
		return units[index].getUnitName();
	}
}
